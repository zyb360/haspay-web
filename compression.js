const fs = require('fs'),
    openFolderPath = './contracts/',
    writeFolderPath = './src/utils/',
    chainId = 56;
// chainId = 8547;

fs.readdir(openFolderPath, (err, jsonNames) => {
    if (!err) {
        const contracts = {};
        jsonNames.forEach(path => {
            if (path !== '.DS_Store') {
                const name = path.slice(0, -5);
                const keys = Object.keys(contracts);
                const contract = require(`${openFolderPath}${path}`);

                if (
                    !keys.includes(name) &&
                    !path.includes('Storage') &&
                    contract.networks[chainId] &&
                    contract.abi.length > 0
                ) {
                    let abi = contract.abi,
                        address = contract.networks[chainId].address;

                    if (jsonNames.includes(name + 'Storage.json')) {
                        const storage = require(`${openFolderPath}${name + 'Storage.json'}`);
                        if (storage.networks[chainId]) {
                            address = storage.networks[chainId].address;
                        }
                    }
                    contracts[name] = { abi, address };
                }
                if (name === 'USDToken') {
                    const abi = contract.abi;
                    const address = '0x55d398326f99059fF775485246999027B3197955';
                    contracts[name] = { abi, address };
                }
            }
        });
        fs.writeFileSync(`${writeFolderPath}contracts.json`, JSON.stringify(contracts));
    } else {
        console.log(err);
    }
});
