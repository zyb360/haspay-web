const cdn = {
    css: [
        'https://cdn.jsdelivr.net/npm/vant@2.11.1/lib/index.css',
    ],
    js: [
        'https://cdn.jsdelivr.net/npm/vue@2.6.11/dist/vue.min.js',
        'https://cdn.jsdelivr.net/npm/amfe-flexible@2.2.1/index.min.js',
        'https://cdn.jsdelivr.net/npm/vue-router@3.2.0/dist/vue-router.min.js',
        'https://cdn.jsdelivr.net/npm/vant@2.11.1/lib/vant.min.js',
        'https://cdn.jsdelivr.net/npm/web3@1.3.4/dist/web3.min.js',
        'https://cdn.jsdelivr.net/npm/vue-clipboard2@0.3.1/dist/vue-clipboard.min.js',
        'https://cdn.jsdelivr.net/npm/echarts@4.6.0/dist/echarts.min.js',
        'https://cdn.jsdelivr.net/npm/axios@0.21.0/dist/axios.min.js',
        'https://cdn.jsdelivr.net/npm/vue-i18n@8.22.3/dist/vue-i18n.min.js',
        
    ]
};

module.exports = {
    publicPath: './',
    productionSourceMap: false,
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].cdn = cdn;
            return args;
        });

        config.plugins.delete('prefetch').delete('preload');
        config.optimization.minimize(true);
    },
    configureWebpack: {
        externals: {
            vue: 'Vue',
            'vue-router': 'VueRouter'
        }
    }
}