const fs = require('fs'),
	openFolderPath = './contracts/',
	writeFolderPath = './src/utils/',
	chainId = 1280;

fs.readdir(openFolderPath, (err, jsonNames) => {
	if (!err) {
		const contracts = {};
		jsonNames.forEach(path => {
			const name = path.slice(0, -5);
			const keys = Object.keys(contracts);
			const contract = require(`${openFolderPath}${path}`);
			// contract.networks[chainId]
			if (!keys.includes(name) && !path.includes('Storage') && contract.abi.length > 0) {
				let abi = contract.abi, address = '';

				this.NFTContractsAddress = '0x122B6077Fb5Be82f5510cc2B642C2d2185F8D7C3';
				if (contract.networks[chainId]) {
					address = contract.networks[chainId].address;
				} else if (name.includes('Consensus')) {
					address = '0x000000000000000000000000000000000000000A';
				} else if (name.includes('Relations')) {
					address = '0x000000000000000000000000000000000000000D';
				} else if (name.includes('CNS')) {
					address = '0x000000000000000000000000000000000000000C';
				}
				else if (name.includes('GenesisMachine')) {
					address = '0x000000000000000000000000000000000000000B';
				}
				else if (name.includes('FeeRecoder')) {
					address = '0xffffffffffffffffffffffffffffffffffffffff';
				}
				else if (name.includes('SystemEventObserver')) {
					address = '0x0000000000000000000000000000000000000000';
				}
				else if (name.includes('Bridge')) {
					address = '0x000000000000000000000000000000000000000E';
				}
				else if (name.includes('AnchorTokensManager')) {
					address = '0x0000000000000000000000000000000000000010';
				}
				else if (name.includes('HRC20Token')) {
					address = '0x1a42A7C62b2A87BC3bC8D0af7A984c362cb2102e';
				}else if(name.includes('ERC721')){
					address='0x122B6077Fb5Be82f5510cc2B642C2d2185F8D7C3'
				}

				if (address.length > 0) {
					contracts[name] = { abi, address };

					fs.writeFileSync(
						`${writeFolderPath}contracts.json`,
						JSON.stringify(contracts)
					);
				}
			}
		})
	} else {
		console.log(err)
	}
})