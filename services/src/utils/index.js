import Contracts from './contracts.json';

export default class Utils {
    address = '';
    constructor(web3) {
        this.web3 = web3;
    }

    initContracts() {
        const keys = Object.keys(Contracts), contracts = {};

        for (const key of keys) {
            contracts[key] = new this.web3.eth.Contract(Contracts[key].abi, Contracts[key].address);
        }

        return contracts;
    }

    async getBalance(address = this.address) {
        return await this.web3.eth.getBalance(address);
    }

    getAccounts() {
        return new Promise(resolve => {
            if (window.imToken) {
                window.imToken.callAPI('user.getCurrentAccount', (err, address) => {
                    if (!err) {
                        this.address = address;
                        resolve(address);
                    }
                })
            } else if (window.ethereum) {
                if (window.ethereum.isTrust || window.ethereum.isHuobi) {
                    this.address = ethereum.address;
                    resolve(ethereum.address);
                } else {
                    ethereum.request({ method: 'eth_requestAccounts' })
                    .then(accounts => {
                        this.address = accounts[0];
                        resolve(accounts[0]);
                    });
                }
            }
        })
    }
}

String.prototype.toWei = toWei;
String.prototype.fromWei = fromWei;
String.prototype.toFixed = toFixed;
String.prototype.toBN = toBN;
String.prototype.utf8ToHex=utf8ToHex
Number.prototype.toWei = toWei;
Number.prototype.fromWei = fromWei;
Number.prototype.toFixed = toFixed;

function toWei(unit = 'ether') {
    return Web3.utils.toWei(`${this}`, unit);
}

function fromWei(unit = 'ether') {
    return Number(Web3.utils.fromWei(this, unit));
}

function utf8ToHex() {
    return new Web3.utils.utf8ToHex(this);
}

function toBN() {
    return new Web3.utils.BN(this);
}

function toFixed(decimals = 2) {
    const _this = typeof this !== 'string' ? `${this}` : this;
    if (!_this.includes('.')) return _this;
    
    const index = _this.indexOf('.');
    if (_this.slice(index + 1).length <= 2) return _this;

    return parseFloat(_this.slice(0, index + 1 + decimals));
}