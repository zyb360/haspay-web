import zh from './zh';
import en from './en';

Vue.use(VueI18n);

export default new VueI18n({
    locale: 'zh',
    messages: { zh, en }
});