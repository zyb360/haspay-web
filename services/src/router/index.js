

Vue.use(VueRouter);

const routes = [];

routes.push(

    {
        path: '/service/zh',
        name: 'zh',
        component: () => import(/* webpackChunkName: 'list' */ '@/views/Service/zh.vue')
    },
    {
        path: '/service/en',
        name: 'en',
        component: () => import(/* webpackChunkName: 'list' */ '@/views/Service/en.vue')
    }
    
     
    
)

const router = new VueRouter({
    mode: 'hash',
    routes
});

export default router;
