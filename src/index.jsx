import { Suspense, useState } from 'react';
import ReactDOM from 'react-dom';
import { ConfigProvider, Layout } from 'antd';
import 'dayjs/locale/zh-cn';
import { Switch, Router, Route, Redirect } from 'react-router-dom';
import { createHashHistory } from 'history';
// import { sendWeb3, sendContracts } from './utils';
import routeConfig from '@routeConfig';
import Home from '@pages/home/index';
import HaloHeader from '@components/header/index';
import HaloFooter from '@components/footer/index';
import { messageWay } from './utils/publicUtils';

import './styles/index.less';
// import TopMenu from './components/header/topMenu/index.jsx';
import './i18n';
import { useTranslation } from 'react-i18next';
const { Content } = Layout;
const history = createHashHistory();
const APP = () => {
    const { t } = useTranslation();
    const [isPC, useisPC] = useState(document.documentElement.offsetWidth > 1200);

    global.isPC = isPC;
    global.t = t;

    // const web3 = async () => {
    //     if (window.ethereum) {
    //         await sendWeb3.eth.getAccounts((err, res) => {
    //             global.$address = res[0];
    //         });
    //     }
    // };
    // web3();

    global.onresize = () => {
        useisPC(document.documentElement.offsetWidth > 1200);
        global.isPC = isPC;
    };
    // if (global.ethereum) {
    //     global.ethereum.on('accountsChanged', accounts => {
    //         if (accounts[0]) {
    //             global.$address = accounts[0];
    //         }
    //         location.reload(true);
    //     });
    //     global.ethereum.on('networkChanged', () => {
    //         location.reload(true);
    //     });
    // }
    // sendWeb3.eth.getChainId().then(async chainId => {
    //     global.chainId = chainId;
    // });

    
    return (
        <ConfigProvider>
            <Router history={history}>
                <Layout className="">
                    {/* <TopMenu></TopMenu> */}
                    <Layout className="home-layout">
                        <HaloHeader isPC={isPC} />
                        <Content>
                            <Suspense fallback="  ">
                                <Switch>
                                    {routeConfig}
                                    <Redirect from="/*" to="/" />
                                    <Route path="/" exact component={Home} />
                                </Switch>
                            </Suspense>
                        </Content>
                    </Layout>
                </Layout>
                <Layout><HaloFooter /></Layout>
            </Router>
        </ConfigProvider>
    );
};

ReactDOM.render(<APP />, document.getElementById('root'));
