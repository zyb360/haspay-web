// eslint-disable-next-line no-use-before-define
import React, { lazy } from 'react';
import { Route } from 'react-router-dom';

const Index = lazy(() => import('@pages/home/index'));

const dapp = lazy(() => import('@pages/dapp/index'));
const token = lazy(() => import('@pages/tokens/index'));

const routes = [
    {
        // 首页
        path: '/',
        component: Index
    },
    {
        path: '/dapp',
        component: dapp
    },
    {
        path: '/token',
        component: token
    }

];

const rootConfig = routes.map(route => {
    const { component: Component } = route;
    return <Route key={route.path} path={route.path} exact component={Component} />;
});
export default rootConfig;
