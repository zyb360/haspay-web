import { Component } from 'react';
import { Button, Input, Row, Col, Form, Select, Radio } from 'antd';
const { TextArea } = Input;

import asImg from '@assets/images/as.png';

import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [
                'ETH', 'BSC',
                'HECO', 'OKExChain', 'HALO'
            ],
            languages: [
                '中文简体',
                '中文繁体',
                'English'
            ],
            isShow: false
        };
    }
    componentDidMount() {
        console.log(localStorage.getItem('lang') == null);
    }

    onChange = () => {

    }
    changeLanguage = () => {

    }
    Open = () => {
        if (this.state.isShow == false) {
            this.setState({
                isShow: true
            });
        } else {
            this.setState({
                isShow: false
            });
        }
    }

    render() {
        const state = this.state;


        return (
            <div className='tokens'>
                <div className='container'>
                    <div className='bg'>
                        Token 入驻申请
                    </div>
                    <Form
                        className='dapp-form'
                        layout="vertical"
                    >
                        <Row>
                            <Col span={24}>
                                <Form.Item
                                    // wrapperCol={{ span: 12 }}
                                    label="项目网络"
                                    required>
                                    <Radio.Group onChange={this.onChange} >
                                        {
                                            state.list.map((item, key) => {
                                                return (
                                                    <Radio key={key} value={key}>{item}</Radio>
                                                );
                                            })
                                        }


                                    </Radio.Group>
                                </Form.Item>
                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item label="代币符号（如：USDT）" required >
                                    <Input placeholder="" />
                                </Form.Item>

                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item
                                    label="合约地址（如：0x4161725d019690a3e0de50f6be67b07a86a9fae1）"
                                    required
                                >
                                    <Input placeholder="" />
                                </Form.Item>
                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item label="精度（如：18）" required >
                                    <Input placeholder="" />
                                </Form.Item>

                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item
                                    label="代币数量"
                                    required
                                >
                                    <Input placeholder="" />
                                </Form.Item>
                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item label="项目简介(0/120)" required >
                                    <Input placeholder="" />
                                </Form.Item>

                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item
                                    label="Email"
                                    required
                                >
                                    <Input placeholder="" />
                                </Form.Item>
                            </Col>
                            <Col span={global.isPC ? 12 : 24}>
                                <Form.Item
                                    label="官方网址（如：https://www.xxx.com）(0/120)"
                                    required
                                >
                                    <Input placeholder="" />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row className='submit'>
                            <Button className=''>提交</Button>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
}
