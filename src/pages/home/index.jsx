import { Component } from 'react';
import { Button, Carousel, Row, Col, message } from 'antd';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';
import bgImg1 from '@assets/home/mobile-bg2.png';

import phone_icon1 from '@assets/home/bg-phone/img_1.png';
// import phone_icon2 from '@assets/home/bg-phone/img_2.png';
import phone_icon3 from '@assets/home/bg-phone/img_3.png';
import phone_icon4 from '@assets/home/bg-phone/img_4.png';
import phone_icon5 from '@assets/home/bg-phone/img_5.png';
import phone_icon6 from '@assets/home/bg-phone/img_6.png';
import phone_icon7 from '@assets/home/bg-phone/img_7.png';
import phone_icon8 from '@assets/home/bg-phone/img_8.png';
import phone_icon9 from '@assets/home/bg-phone/img_9.png';
import phone_icon10 from '@assets/home/bg-phone/img_10.png';

import phone from '@assets/home/iphone.png';
import Android from '@assets/home/Android.png';
import icon1 from '@assets/home/icon1.png';
import icon2 from '@assets/home/icon2.png';
import icon3 from '@assets/home/icon3.png';

import img2 from '@assets/home/img2.png';
import token1 from '@assets/home/token1.png';
import token2 from '@assets/home/token2.png';
import token3 from '@assets/home/token3.png';
import token4 from '@assets/home/token4.png';
import token5 from '@assets/home/token5.png';
import token6 from '@assets/home/token6.png';
import token7 from '@assets/home/token7.png';
import token8 from '@assets/home/token8.png';
import token9 from '@assets/home/token9.png';
import token10 from '@assets/home/token10.png';
import token11 from '@assets/home/token11.png';
import token12 from '@assets/home/token12.png';
import img3_1 from '@assets/home/img3_1.png';
import img3_2 from '@assets/home/img3_2.png';
import img3_3 from '@assets/home/img3_3.png';

import ethImg from '@assets/home/module4/eth.png';
import bnbImg from '@assets/home/module4/bnb.png';
import haloImg from '@assets/home/module4/halo.png';
import hecoImg from '@assets/home/module4/heco.png';

import fantomImg from '@assets/home/module4/ftm.png';
import kccImg from '@assets/home/module4/par_kcc.png';
import polygonImg from '@assets/home/module4/par_polygon.png';
import avaxImg from '@assets/home/module4/avax.png';
import bttImg from '@assets/home/module4/btt.png';
import arbiImg from '@assets/home/module4/arbi.png';
import optimImg from '@assets/home/module4/optim.png';
import moonImg from '@assets/home/module4/moon.png';

import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lists: [
                {
                    src: icon1,
                    title: global.t('home.text5'),
                    content: global.t('home.text6')
                },
                {
                    src: icon2,
                    title: global.t('home.text7'),
                    content: global.t('home.text8')
                },
                {
                    src: icon3,
                    title: global.t('home.text9'),
                    content: global.t('home.text10')
                }
            ]
        };
    }
    componentDidMount() {
        console.log(localStorage.getItem('lang') == null);
    }
    down = () => {
        window.scrollTo({ top: document.getElementById('btn').offsetTop, behavior: 'smooth' });
    };
    downLoad = e => {
        if (e == 1) {
            // message.destroy();
            // return message.open({
            //     className: 'info-class',
            //     style: {
            //         marginTop: '20vh'
            //     },
            //     duration: 2,
            //     content: t('tips.message1')
            // });
            window.open('https://itunes.apple.com/app/6472152170');
        } else if (e == 2) {
            //https://github.com/hashpaypie/Mobileterminal/raw/master/hashpay_latest.apk c
            window.location.href = 'https://hashpay.pro/mobileterminal/hashpay_latest.apk';
        } else if (e == 3) {
            window.open('https://play.google.com/store/apps/details?id=com.wallet_hashpay_V2');
        }
    };
    render() {
        const state = this.state;

        const topBg = global.isPC ? '' : bgImg1;
        return (
            <div className={global.isPC ? 'home' : 'home-mobile'}>
                <div
                    className="top-bg"
                    style={global.isPC ? {} : { backgroundImage: `url(${bgImg1})` }}
                >
                    {global.isPC ? (
                        <video
                            id="homeVideo"
                            className=" video-js vjs-default-skin vjs-big-play-centered example_video"
                            muted
                            autoPlay
                            loop
                            preload="none"
                            width="640"
                            height="264"
                            poster={require('@assets/home/bg1.jpg')}
                            data-setup="{}"
                            src={require('@assets/audio/top.mp4')}
                        >
                            {/* <source src={require('@assets/audio/top.mp4')} type='video/mp4' /> */}
                        </video>
                    ) : (
                        ''
                    )}
                    <div className="container content" id="scrollTop">
                        <div
                            className={
                                localStorage.getItem('lang') == 'zh_CN' ? 'title' : 'other-title'
                            }
                        >
                            {global.t('home.text1')} &nbsp;&nbsp;{global.t('home.text2')}
                        </div>
                        <div
                            className={
                                localStorage.getItem('lang') == 'zh_CN'
                                    ? 'content'
                                    : 'other-content'
                            }
                        >
                            {global.t('home.text3')}
                        </div>
                        {global.isPC ? (
                            ''
                        ) : (
                            <div
                                className="box"
                                style={
                                    {
                                        // backgroundImage: `url(${shou})`
                                    }
                                }
                            >
                                <div>
                                    <img className="icon10 jump" src={phone_icon10}></img>
                                    <img className="icon6 jump" src={phone_icon6}></img>
                                </div>
                                <div>
                                    <img className="icon1 jump" src={phone_icon1}></img>
                                    <div>
                                        <img className="icon3 jump" src={phone_icon3}></img>
                                        <img className="icon4 jump" src={phone_icon4}></img>
                                    </div>
                                    <img className="icon5 jump" src={phone_icon5}></img>
                                </div>
                                <div className="icon-right">
                                    <img className="icon7 jump" src={phone_icon7}></img>
                                    <img className="icon8 jump" src={phone_icon8}></img>
                                    <img className="icon9 jump" src={phone_icon9}></img>
                                </div>
                            </div>
                        )}
                        <div className="btn" id="btn">
                            <Button onClick={this.downLoad.bind(this, 1)}>
                                <div>
                                    <img src={phone}></img>IPhone {global.t('home.text4')}
                                </div>
                            </Button>
                            <Button onClick={this.downLoad.bind(this, 2)}>
                                <div>
                                    <img src={Android}></img>Android {global.t('home.text4')}
                                </div>
                            </Button>
                            <Button onClick={this.downLoad.bind(this, 3)}>
                                <div>
                                    <img src={require('@assets/home/pay.png')}></img>Google Play{' '}
                                    {global.t('home.text4')}
                                </div>
                            </Button>
                        </div>
                        {/* {
                            global.isPC ?
                                <div className='box'
                                style={{
                                    // backgroundImage: `url(${shou})`

                                }}
                                >
                                    <div>
                                        <img className='icon10 jump' src={phone_icon10}></img>
                                        <img className='icon6 jump' src={phone_icon6}></img>
                                    </div>
                                    <div>
                                        <img className='icon1 jump' src={phone_icon1}></img>
                                        <div>
                                            <img className='icon3 jump' src={phone_icon3}></img>
                                            <img className='icon4 jump' src={phone_icon4}></img>
                                        </div>
                                        <img className='icon5 jump' src={phone_icon5}></img>
                                    </div>
                                    <div className='icon-right'>
                                        <img className='icon7 jump' src={phone_icon7}></img>
                                        <img className='icon8 jump' src={phone_icon8}></img>
                                        <img className='icon9 jump' src={phone_icon9}></img>
                                    </div>
                                </div> : ''
                        } */}
                    </div>
                </div>
                <div className="module1">
                    <div className="container">
                        {global.isPC ? (
                            <Row>
                                {state.lists.map((item, key) => {
                                    return (
                                        <Col span={8} key={key}>
                                            <img className="jumpHover" src={item.src}></img>
                                            <div className="title">{item.title}</div>
                                            <div className="content">{item.content}</div>
                                        </Col>
                                    );
                                })}
                            </Row>
                        ) : (
                            <Swiper
                                className="module1-swiper"
                                slidesPerView={1.2}
                                spaceBetween={20}
                            >
                                <div className="swiper-wrapper">
                                    {state.lists.map((item, key) => {
                                        return (
                                            <SwiperSlide key={key}>
                                                <div
                                                    className={
                                                        localStorage.getItem('lang') == null ||
                                                        localStorage.getItem('lang') == 'zh_CN'
                                                            ? 'item'
                                                            : 'other-item'
                                                    }
                                                >
                                                    <img src={item.src}></img>
                                                    <div className="title">{item.title}</div>
                                                    <div className="content">{item.content}</div>
                                                </div>
                                            </SwiperSlide>
                                        );
                                    })}
                                </div>
                            </Swiper>
                        )}
                    </div>
                </div>
                <div className="module2">
                    <div className="container">
                        {global.isPC ? (
                            ''
                        ) : (
                            <div className="top">
                                <div className="title">{global.t('home.text11')}</div>
                                <div className="small-line"></div>
                            </div>
                        )}
                        <img className="jumpHover" src={img2}></img>
                        <div className="right">
                            {global.isPC ? (
                                <div className="top">
                                    <div className="title">{global.t('home.text11')}</div>
                                    <div className="small-line"></div>
                                </div>
                            ) : (
                                ''
                            )}
                            <div className="content1">{global.t('home.text12')}</div>
                            <div className="content2">
                                <div>
                                    <div>12</div>
                                    <div>{global.t('home.text13')}</div>
                                </div>

                                <div>
                                    <div>2000K</div>
                                    <div>{global.t('home.text14')}</div>
                                </div>
                            </div>
                            <div className="content3">{global.t('home.text15')}</div>
                            <div className="tokens">
                                <div>
                                    <img src={token1}></img>
                                    <img src={token2}></img>
                                    <img src={token3}></img>
                                    <img src={token4}></img>
                                    <img src={token5}></img>
                                    <img src={token6}></img>
                                </div>
                                <div>
                                    <img src={token7}></img>
                                    <img src={token8}></img>
                                    <img src={token9}></img>
                                    <img src={token10}></img>
                                    <img src={token11}></img>
                                    <img src={token12}></img> <span>...</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="module3">
                    <div className="container">
                        {global.isPC ? (
                            <div className="block1">
                                <div className="content">
                                    <div className="title">{global.t('home.text16')}</div>
                                    <div className="small-line"></div>
                                    <div className="text1">{global.t('home.text17')}</div>
                                    <div className="text2">{global.t('home.text18')}</div>
                                </div>
                                <img className="jumpHover" src={img3_1}></img>
                            </div>
                        ) : (
                            <div className="block1">
                                <div className="top">
                                    <div className="title">{global.t('home.text16')}</div>
                                    <div className="small-line"></div>
                                </div>
                                <img src={img3_1}></img>
                                <div className="content">
                                    <div className="text1">{global.t('home.text17')}</div>
                                    <div className="text2">{global.t('home.text18')}</div>
                                </div>
                            </div>
                        )}
                        <div className="block2">
                            {global.isPC ? (
                                ''
                            ) : (
                                <div className="top">
                                    <div className="title">{global.t('home.text19')}</div>
                                    <div className="small-line"></div>
                                </div>
                            )}
                            <img className="jumpHover" src={img3_2}></img>
                            <div className="content">
                                {global.isPC ? (
                                    <div className="top">
                                        <div className="title">{global.t('home.text19')}</div>
                                        <div className="small-line"></div>
                                    </div>
                                ) : (
                                    ''
                                )}
                                <div className="text1">{global.t('home.text20')}</div>
                                <div className="text2">{global.t('home.text21')}</div>
                            </div>
                        </div>
                        {global.isPC ? (
                            <div className="block3">
                                <div className="content">
                                    <div className="title">{global.t('home.text22')}</div>
                                    <div className="small-line"></div>
                                    <div className="text1">{global.t('home.text23')}</div>
                                </div>
                                <img src={img3_3} className="jumpHover"></img>
                            </div>
                        ) : (
                            <div className="block3">
                                <div className="top">
                                    <div className="title">{global.t('home.text22')}</div>
                                    <div className="small-line"></div>
                                </div>
                                <img src={img3_3}></img>
                                <div className="content">
                                    <div className="text1">{global.t('home.text23')}</div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className="module4">
                    <div className="container">
                        <div className="title">{global.t('home.text24')}</div>
                        <Row className="same-column">
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://etherscan.io/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={ethImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a href="https://bscscan.com/" target="_blank" rel="noreferrer">
                                        <img src={bnbImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://browser.halo.land/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={haloImg} className="halo"></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://scan.hecochain.com/en-us/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={hecoImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a href="https://ftmscan.com" target="_blank" rel="noreferrer">
                                        <img src={fantomImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://bttcscan.com/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={bttImg}></img>
                                    </a>
                                </div>
                            </Col>

                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://polygonscan.com"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={polygonImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://optimistic.etherscan.io/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={optimImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://www.avax.network/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={avaxImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a href="https://arbiscan.io/" target="_blank" rel="noreferrer">
                                        <img src={arbiImg}></img>
                                    </a>
                                </div>
                            </Col>

                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://moonbeam.moonscan.io/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img src={moonImg}></img>
                                    </a>
                                </div>
                            </Col>
                            <Col span={global.isPC ? 6 : 8}>
                                <div>
                                    <a
                                        href="https://explorer.kcc.io/"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img className="kcc" src={kccImg}></img>
                                    </a>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}
