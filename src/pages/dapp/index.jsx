import { Component } from 'react';
import { Button, Input, Row, Col, Form, Select } from 'antd';
const { TextArea } = Input;

import asImg from '@assets/images/as.png';
import haspay from '@assets/icon/icon-hashpay.png';
import './index.less';

export default class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [
                'ETH', 'BSC',
                'HECO'
            ],
            languages: [
                '中文简体',
                '中文繁体',
                'English'
            ],
            isShow: false
        };
    }
    componentDidMount() {
        console.log(localStorage.getItem('lang') == null);


    }

    onGenderChange = () => {

    }
    changeLanguage = () => {

    }
    Open = () => {
        if (this.state.isShow == false) {
            this.setState({
                isShow: true
            });
        } else {
            this.setState({
                isShow: false
            });
        }
    }

    render() {
        const state = this.state;


        return (
            <div className='dapp'>
                <div className='container'>
                    <div className='bg'>
                        Dapp 入驻申请
                    </div>
                    <Form
                        className='dapp-form'
                        layout="vertical"
                    >
                        <Row>
                            <Col span={global.isPC? 12:24}>
                                <Form.Item
                                    // wrapperCol={{ span: 12 }}
                                    label="项目网络"
                                    required>
                                    <Select
                                        placeholder=""
                                        onChange={this.onGenderChange}
                                        allowClear
                                    >
                                        {
                                            state.list.map((item, key) => {
                                                return (
                                                    <Select.Option key={key} value={key}>{item}</Select.Option>
                                                );
                                            })
                                        }


                                    </Select>
                                </Form.Item>

                            </Col>
                            <Col span={global.isPC? 12:24}>
                                <Form.Item
                                    // wrapperCol={{ span: 12 }}
                                    label="项目语言"
                                    required>
                                    <Select
                                        placeholder=""
                                        onChange={this.changeLanguage}
                                        allowClear
                                    >
                                        {
                                            state.languages.map((item, key) => {
                                                return (
                                                    <Select.Option key={key} value={key}>{item}</Select.Option>
                                                );
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={global.isPC? 12:24}>
                                <Form.Item label="项目名称 (0/60)" required >
                                    <Input placeholder="" />
                                </Form.Item>

                            </Col>
                            <Col span={global.isPC? 12:24} >
                                <Form.Item
                                    label="项目链接 (0/250)"
                                    required
                                >
                                    <Input placeholder="" />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={global.isPC? 12:24}>
                                <Form.Item label="项目简介 (0/100)" required>
                                    <TextArea rows={7} />
                                </Form.Item>
                            </Col>
                            <Col span={global.isPC? 12:24}>
                                <Form.Item label="DApp图标 (图片大小：200x200px，支持JPG、PNG)" style={{ float: 'left' }}>
                                </Form.Item>
                                <div className='example'>
                                    <div className='pointer' onClick={this.Open}>示例</div>
                                    {
                                        state.isShow ?
                                            <div className='imgWrap'>
                                                <img src={haspay}></img>
                                            </div> : ''
                                    }
                                </div>


                            </Col>
                        </Row>
                        <Row className='submit'>
                            <Button className=''>提交</Button>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
}
