// import { useState, useEffect } from 'react';
// import { useTranslation } from 'react-i18next';
// import { Select } from 'antd';
// import MenuItem from 'antd/lib/menu/MenuItem';
// import { useLocation } from 'react-router-dom';
import logo from '@assets/logo1.png';
import icon1 from '@assets/footer/icon1.png';
import icon2 from '@assets/footer/icon2.png';
import icon3 from '@assets/footer/icon3.png';
import icon4 from '@assets/footer/icon4.png';
import icon5 from '@assets/footer/icon5.png';
import icon6 from '@assets/footer/icon6.png';
import icon7 from '@assets/footer/icon7.png';
import icon8 from '@assets/footer/icon8.png';

import ChangeLanguage from '@components/changeLanguage/index';
import './index.less';
function HaloFooter(props) {

    const changeMenu = e => {
        if (e == 'token') {
            window.open('https://hashpayinfo.gitbook.io/submit-token/');
        } else if (e == 'dapp') {
            window.open('https://hashpayinfo.gitbook.io/submit-token/');
        }
        else {
            window.location.href = e;
        }

    };

    return (
        <div className="footer" id="footer">
            <div className="container">
                {global.isPC ? (
                    <div className="footer-pc">
                        <div className="top">
                            <div className="left">
                                <img src={logo}></img>
                                <div>{global.t('footer.text1')}</div>
                                <div className='icon'>
                                    {/* <a><img src={icon1}></img></a>
                                    <a><img src={icon2}></img></a>
                                    <a><img src={icon3}></img></a>
                                    <a><img src={icon4}></img></a>
                                    <a><img src={icon5}></img></a>
                                    <a><img src={icon6}></img></a>
                                    <a><img src={icon7}></img></a> */}
                                    <a href='https://hashpayinfo.gitbook.io/submit-token/' target='_blank'><img src={icon8}></img></a>
                                </div>
                            </div>
                            <div className="right">
                                <div>
                                    <div className="title">{global.t('footer.text2')}</div>
                                    <div onClick={changeMenu.bind(this, '')}>
                                        {global.t('footer.text3')}
                                    </div>
                                    {/* <div onClick={changeMenu.bind(this, '#/nftMarket')}>
                                        {global.t('footer.text4')}
                                    </div> */}

                                </div>
                                <div>
                                    <div className="title">{global.t('footer.text5')}</div>
                                    <div onClick={changeMenu.bind(this, 'token')}>
                                        {global.t('menu.SubmitToken')}
                                    </div>
                                    {/* <div onClick={changeMenu.bind(this, 'dapp')}>
                                        {global.t('menu.SubmitDapp')}
                                    </div> */}
                                </div>
                                <div>
                                    <div className="title">{global.t('footer.text8')}</div>
                                    <div onClick={changeMenu.bind(this, '')}>

                                        {global.t('footer.text9')}
                                    </div>
                                    <div onClick={changeMenu.bind(this, '')}>
                                        {global.t('footer.text10')}
                                    </div>
                                </div>
                                <div>
                                    <div className="title">
                                        {global.t('Language')}
                                    </div>
                                    <ChangeLanguage></ChangeLanguage>
                                </div>
                                <div></div>
                            </div>
                        </div>
                        <div className="bottom">

                            <div>
                                ©2022 HashPay PTE. LTD. All rights reserved.
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="footer-mobile">
                        <div className='icon'>
                            {/* <a><img src={icon1}></img></a>
                            <a><img src={icon2}></img></a>
                            <a><img src={icon3}></img></a>
                            <a><img src={icon4}></img></a>
                            <a><img src={icon5}></img></a>
                            <a><img src={icon6}></img></a>
                            <a><img src={icon7}></img></a> */}
                            <a href='https://hashpayinfo.gitbook.io/submit-token/' target='_blank'><img src={icon8}></img></a>
                        </div>
                        <div className="content"> ©2022 HashPay PTE. LTD. All rights reserved.</div>
                    </div>
                )}
            </div>

        </div>
    );
}

export default HaloFooter;
