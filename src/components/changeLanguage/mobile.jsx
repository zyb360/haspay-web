import { useState, useRef, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { message, Select, Radio, Space } from 'antd';
import { useTranslation } from 'react-i18next';
import img from '@assets/images/language.png';
import './mobile.less';

function topMenu(props) {
    const { pathname } = useLocation();
    const { t, i18n } = useTranslation();
    const [value, setValue] = useState(
        localStorage.getItem('lang') ? localStorage.getItem('lang') : 'zh_CN'
    );
    const childrenRef = useRef();

    const changeLanguages = e => {
        console.log(e);
        i18n.changeLanguage(e.target.value);
        setValue(e.target.value);
        localStorage.setItem('lang', e.target.value);
    };

    return (
        <div className="mobile-language" id="">
            <span>{global.t('language')}</span>
            <img src={img}></img>
            <div>
                {/* <Select
                    defaultValue={
                        localStorage.getItem('lang') ? localStorage.getItem('lang') : 'en'
                    }
                    onChange={changeLanguages}
                    getPopupContainer={() =>
                        document.getElementById('language-change')
                    }
                >
                    <Select.Option value="zh_CN">{t('chinese')}</Select.Option>
                    <Select.Option value="en">{t('english')}</Select.Option>
                </Select> */}
                <Radio.Group onChange={changeLanguages} value={value}>
                    <Radio value="zh_CN" className="radio-item">
                        <img src={require('@assets/images/china.png')}></img>
                        {global.t('chinese')}
                    </Radio>
                    <Radio value="en" className="radio-item">
                        <img src={require('@assets/images/english.png')}></img>
                        {global.t('english')}
                    </Radio>

                    <Radio value="ja" className="radio-item">
                        <img src={require('@assets/images/china.png')}></img>
                        {global.t('ja')}
                    </Radio>
                    <Radio value="ko" className="radio-item">
                        <img src={require('@assets/images/english.png')}></img>
                        {global.t('ko')}
                    </Radio>
                    <Radio value="ru" className="radio-item">
                        <img src={require('@assets/images/china.png')}></img>
                        {global.t('ru')}
                    </Radio>
                </Radio.Group>
            </div>
        </div>
    );
}
export default topMenu;
