// import { useState, useRef } from 'react';
// import { useState } from 'react';
// import { useLocation } from 'react-router-dom';
import { Select } from 'antd';
import { useTranslation } from 'react-i18next';
import './index.less';

function topMenu(_props) {
    
    const { t, i18n } = useTranslation();
   

    const changeLanguages = e => {
        i18n.changeLanguage(e);
        localStorage.setItem('lang', e);
        window.location.reload();
    };

    return (
        <div className="language-change" id="language-change">
            <div>
                <Select
                    className="language"
                    defaultValue={
                        localStorage.getItem('lang') ? localStorage.getItem('lang') : 'zh_CN'
                    }
                    onChange={changeLanguages}
                    getPopupContainer={() => document.getElementById('language-change')}
                >
                    <Select.Option value="zh_CN">{t('chinese')}</Select.Option>
                    <Select.Option value="en">{t('english')}</Select.Option>
                    {/* <Select.Option value="ja">{t('ja')}</Select.Option>
                    <Select.Option value="ko">{t('ko')}</Select.Option>
                    <Select.Option value="ru">{t('ru')}</Select.Option> */}
                </Select>
            </div>
        </div>
    );
}
export default topMenu;
