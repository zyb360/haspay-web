import imtokenIcon from '@assets/icon/imtoken.png';
import metamasksIcon from '@assets/icon/metamasks.png';
import tokenpokectsIcon from '@assets/icon/tokenpokects.png';
import mbg4Img from '@assets/images/m_hashpay_s_b.png';
import { Button, message, Modal, Select } from 'antd';
import { Component } from 'react';
import { sendWeb3, setSessionStorage } from '../../../utils';
import '../../../styles/index.less';
const { Option } = Select;
export default class ConfimModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            //
            walletType: 'Metamask',
            chainId: '',
            connectState: ''
        };

        if (props.onRef) {
            props.onRef(this);
        }
    }

    componentDidMount() {
        if (window.ethereum) {
            window.ethereum
                .request({ method: 'eth_requestAccounts' })
                .then(accounts => {
                    if (accounts[0]) {
                        global.$address = accounts[0];
                    }

                    sessionStorage.setItem('walletType', 'Metamask');
                    this.setState({ visible: false });
                })
                .catch(err => {
                    console.error(err);
                });
        }
    }

    handleChange = value => {
        this.setState({ walletType: value });
    };
    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = () => {
        this.setState({ visible: false });
    };

    handleOK = () => {
        this.setState({ visible: false });
    };
    connectWallet = async () => {
        // useConnectState('connecting');
        let chainId = await sendWeb3.eth.getChainId();
        if (window.$address == 'undefined') {
            message.error({
                content: global.t('tips.message4'),
                style: {
                    marginTop: '20vh'
                }
            });
            return;
        }
        if (chainId !== 1 && chainId !== 1280 && chainId !== 56) {
            message.error({
                content: global.t('tips.message'),
                className: 'custom-class',
                style: {
                    marginTop: '20vh'
                }
            });
            // useConnectState('connect');
        } else {
            if (this.state.walletType === 'Metamask') {
                console.log(111);
                if (!Boolean(window.ethereum.isMetaMask)) {
                    console.log(122);
                    return message.error({
                        content: global.t('tips.message5'),
                        className: 'custom-class',
                        style: {
                            marginTop: '20vh'
                        }
                    });
                }
                global.ethereum
                    .request({ method: 'eth_requestAccounts' })
                    .then(accounts => {
                        if (accounts[0]) {
                            global.$address = accounts[0];
                        }

                        message.success({
                            content: global.t('tips.success'),
                            className: 'custom-class',
                            style: {
                                marginTop: '20vh'
                            }
                        });
                        sessionStorage.setItem('walletType', 'Metamask');
                        // useConnectState('connected');
                        this.setState({ visible: false });
                        location.reload();
                    })
                    .catch(err => {
                        console.error(err);
                    });
            } else {
                console.log(222);
                sendWeb3.eth
                    .getAccounts()
                    .then(accounts => {
                        if (accounts[0]) {
                            global.$address = accounts[0];
                        }
                        message.success({
                            content: global.t('tips.success'),
                            className: 'custom-class',
                            style: {
                                marginTop: '20vh'
                            }
                        });
                        // useConnectState('connected');
                        sessionStorage.setItem('walletType', '');
                        this.setState({ visible: false });
                        location.reload();
                    })
                    .catch(err => {
                        global.alert('error');
                        global.alert(err);
                        console.error(err);
                    });
            }
        }
    };

    render() {
        return (
            <Modal
                className={global.isMobile ? 'from-modal' : 'mobile-from-modal'}
                title=""
                visible={this.state.visible}
                closable={false}
                footer={null}
            >
                <Select className="right-sel" defaultValue="Metamask" onChange={this.handleChange}>
                    <Option value="Metamask">
                        <div className={global.isMobile ? 'option-item' : 'mobile-option-item'}>
                            <img src={metamasksIcon} />
                            {/* <span>Metamask</span> */}
                        </div>
                    </Option>
                    <Option value="Hashpay">
                        <div className={global.isMobile ? 'option-item' : 'mobile-option-item'}>
                            <img src={mbg4Img} />
                            {/* <span>Hashpay</span> */}
                        </div>
                    </Option>
                    <Option value="tokenPocket">
                        <div className={global.isMobile ? 'option-item' : 'mobile-option-item'}>
                            <img src={tokenpokectsIcon} />
                            {/* <span>Hashpay</span> */}
                        </div>
                    </Option>
                    <Option value="imToken">
                        <div className={global.isMobile ? 'option-item' : 'mobile-option-item'}>
                            <img src={imtokenIcon} />
                            {/* <span>Hashpay</span> */}
                        </div>
                    </Option>
                </Select>
                <div className="modal-bt">
                    <a
                        className="btn-m-w"
                        onClick={() => {
                            this.setState({ visible: false });
                        }}
                    >
                        {global.t('CANCEL')}
                    </a>
                    <Button type="primary" className="btn-m" onClick={this.connectWallet}>
                        {global.t('ConnectWallet')}
                    </Button>
                </div>
            </Modal>
        );
    }
}
