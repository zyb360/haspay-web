import { useState, useRef, useEffect } from 'react';
import { withRouter, useLocation } from 'react-router-dom';
import { Button, message, Modal, Select } from 'antd';
import { getSessionStorage } from '../../../utils';
import '../../../styles/index.less';
import Children from '../popus/popus.jsx';
function topMenu() {
    const [address, useAddress] = useState(getSessionStorage('address'));

    const { pathname } = useLocation();
    const [isurl, useIsurl] = useState(false);
    const childrenRef = useRef();
    const openModal = () => {
        childrenRef.current.showModal();
    };
    useEffect(() => {
        if (
            pathname == '/bridgePage' ||
            pathname == '/pos' ||
            pathname == '/posHistory' ||
            pathname == '/hashrateDetail'
        ) {
            useIsurl(true);
        } else {
            useIsurl(false);
        }
    }, [pathname]);
    return (
        <div>
            {
                // (pathname == '/bridgePage' || pathname == '/pos' || pathname == '/posHistory' || pathname == '/hashrateDetail')?(
                //     global.isMobile && location.hash != '#/' ? (
                //         <div className="oneTop">
                //             <div>
                //                 {global.$address ? (
                //                     <div className="left">
                //                         <div>
                //                             <div className="top">
                //                                 {/* {Boolean(window.ethereum.isMetaMask) ? (
                //                                         <img src={bridgeMaskLogo} />
                //                                     ) : (
                //                                         <img src={bridgeHashpay} />
                //                                     )} */}
                //                                 <div>{global.$address.slice(0, 5) + '...' + global.$address.slice(-5)}</div>
                //                             </div>
                //                             <div className="bottom">
                //                                 <div>
                //                                     <div></div>
                //                                 </div>
                //                                 <div>
                //                                     {global.chainId == 1
                //                                         ? 'Ethereum NETWORK'
                //                                         : global.chainId == 56
                //                                             ? 'Binance Smart Chain Network'
                //                                             : 'HALO NETWORK'}
                //                                 </div>
                //                             </div>
                //                         </div>
                //                         {pathname != '#/bridgePage' && pathname != '#/pos' && !isurl ? (
                //                             <Button type="primary" onClick={openModal}>
                //                                 {global.t('CHANGEWALLET')}
                //                             </Button>
                //                         ) : (
                //                             ''
                //                         )}
                //                     </div>
                //                 ) : (
                //                     <div>
                //                         {pathname != '#/bridgePage' && pathname != '#/pos' && !isurl ? (
                //                             <Button type="primary" onClick={openModal}>
                //                                 {global.t('ConnectWallet')}
                //                             </Button>
                //                         ) : (
                //                             ''
                //                         )}
                //                     </div>
                //                 )}
                //             </div>
                //         </div>
                //     ) : (
                //         ''
                //     )
                // ):(
                global.isMobile && location.hash != '#/' ? (
                    <div className="oneTop" style={!global.$address ? { lineHeight: '30px' } : {}}>
                        <div>
                            {global.$address ? (
                                <div className="left">
                                    <div>
                                        <div className="top">
                                            {/* {Boolean(window.ethereum.isMetaMask) ? (
                                                        <img src={bridgeMaskLogo} />
                                                    ) : (
                                                        <img src={bridgeHashpay} />
                                                    )} */}
                                            <div>
                                                {global.$address.slice(0, 5) +
                                                    '...' +
                                                    global.$address.slice(-5)}
                                            </div>
                                        </div>
                                        <div className="bottom">
                                            <div>
                                                <div></div>
                                            </div>
                                            <div>
                                                {global.chainId == 1
                                                    ? 'Ethereum NETWORK'
                                                    : global.chainId == 56
                                                    ? 'Binance Smart Chain Network'
                                                    : 'HALO NETWORK'}
                                            </div>
                                        </div>
                                    </div>

                                    {/* <Button type="primary" onClick={openModal}>
                                        {global.t('CHANGEWALLET')}
                                    </Button> */}
                                </div>
                            ) : (
                                <div>
                                    {/* <Button type="primary" onClick={openModal}>
                                        {global.t('ConnectWallet')}
                                    </Button> */}
                                    {<div style={{ color: '#bbb' }}> ● 未连接 </div>}
                                </div>
                            )}
                        </div>
                    </div>
                ) : (
                    ''
                )
                // )
            }

            <Children ref={childrenRef}></Children>
        </div>
    );
}
export default topMenu;
